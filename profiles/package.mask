# Bryan Gardiner <bog@khumba.net> (2024-01-25)
# qalculate-qt now exists in ::gentoo, please use the official ebuild.
# Masked for removal.
sci-calculators/qalculate-qt

# Bryan Gardiner <bog@khumba.net> (2023-12-25)
# This ebuild provides a general patch for NetworkManager under
# /etc/portage/patches.  Please read the long description in
# metadata.xml before unmasking.  Using equery from gentoolkit:
#
#     equery meta -d net-misc/networkmanager-no-modify-system-patch
net-misc/networkmanager-no-modify-system-patch

# Bryan Gardiner <bog@khumba.net> (2023-12-22)
# Tagaini Jisho is again up to date in ::gentoo, please switch back to the
# official Gentoo ebuild.
app-i18n/tagainijisho

# Bryan Gardiner <bog@khumba.net> (2022-11-08)
# Masked due to bug which breaks all GTK+ programs with >=glib-2.74.0.
# Please upgrade to gtk3-classic-patches-3.24.34.2.
# See https://github.com/lah7/gtk3-classic/issues/79.
<x11-libs/gtk3-classic-patches-3.24.34.2
